# Linux Kernel config
## How to use
1. Ensure your system is UEFI ready
2. View NVRAM with efibootmgr:
```sh
efibootmgr -v
```
3. Edit .config in kernel directory and compile the kernel:
```sh
make menuconfig
make -j8
```
4. Copy kernel binary to EFI/your\_label\_name and install modules if any:
```sh
cp arch/x86/boot/bzImage /boot/EFI/your_label_name/vmlinuz-linux.efi
make -j4 modules_install
```
5. Add NVRAM entry:
```sh
efibootmgr -c -p 1 -L your_label_name -l \\EFI\\your_label_name\\vmlinuz-linux.efi -u "root=/dev/sdXY rootfstype=ext4 (other kernel cmdline params)"
```
6. Change EFI boot order:
```sh
efibootmgr -o XXXX,YYYY...
```
7. (Optional) Boot system from EFI menu
## Notes
- You should embed in linux image (say Y) needed kernel drivers, AHCI, SCSI (sd\_mod) etc
- Remove initrd support from kernel image
- Don't use UUIDs, replace them with absolute paths in dev directory (fstab and root parameter in linux cmdline)
- System must have linux\_firmware package installed
